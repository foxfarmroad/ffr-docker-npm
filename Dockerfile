FROM docker:stable as docker

FROM node:8
COPY --from=docker /usr/local/bin/ /usr/local/bin/

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["sh"]